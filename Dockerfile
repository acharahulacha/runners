FROM python:3.8.0-slim
WORKDIR /app
ADD . /app
RUN pip3 install --trusted-host pypi.python.org Flask
ENV NAME Rahul
CMD ["python","app.py"]
